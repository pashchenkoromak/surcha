#include "GeometryHelper.h"
#include <cmath>

bool isClose(double a, double b, double eps /* = EPS */)
{
	return abs(a - b) < eps;
}

double sqr(double a)
{
	return a * a;
}

Point MovePoint(Point from, Point to, const double speed, const double dt)
{
	Point pos = from;
	auto dist = Distance(to, from);
	if (dist < speed * dt) {
		pos = to;
	} else {
		pos = pos + (to - pos) / (to - pos).length() * speed * dt;
	}
	return pos;
}

template <typename T>
TPoint<T>::TPoint() : x(0), y(0)
{
}

template <typename T>
TPoint<T>::TPoint(T _x, T _y) : x(_x), y(_y)
{
}

template <typename T>
inline double TPoint<T>::length() const
{
	return sqrt(sqr(x) + sqr(y));
}

template <typename T>
bool TPoint<T>::operator<(const TPoint<T> &rhs) const
{
	if (x == rhs.x)
		return y < rhs.y;
	else
		return x < rhs.x;
}

template <typename T>
bool TPoint<T>::operator==(const TPoint<T> &rhs) const
{
	return x == rhs.x && y == rhs.y;
}
template <typename T>
bool TPoint<T>::operator!=(const TPoint<T> &rhs) const
{
	return x != rhs.x || y != rhs.y;
}

template <typename T>
int sign(T a)
{
	if (a < 0)
		return -1;
	if (a > 0)
		return 1;
	return 0;
}

template <typename T>
int GetClosestInteger(T a)
{
	if (fabs(a - (int)a) >= 0.5) {
		return (int)a + sign(a);
	}
	return a;
}

template <typename T>
IPoint TPoint<T>::GetClosestIntPoint() const
{
	return IPoint(GetClosestInteger(x), GetClosestInteger(y));
}

template class TPoint<double>;
template class TPoint<int>;
