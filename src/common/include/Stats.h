#pragma once
#include "GeometryHelper.h"
#include <magic_enum.hpp>
#include <glog/logging.h>
#include <nlohmann/json.hpp>
#include <map>
#include <variant>

constexpr double NOT_INITED = -1000000.0;
typedef std::variant<std::monostate, double, bool> PossibleType;

inline bool IsInitialized(PossibleType value)
{
	return std::holds_alternative<double>(value) || std::holds_alternative<bool>(value);
}

enum class eUnits { PhysicalObject, BaseUnit, BaseArcher, BaseMainStructure };
enum class eProjectives { PhysicalObject, BaseProjective };

template <class StatsEnum>
struct StatsHolder {
	StatsHolder()
	{
		stats.resize(magic_enum::enum_count<StatsEnum>());
	}
	void SetStats(StatsEnum stat, PossibleType value)
	{
		stats[*magic_enum::enum_index<StatsEnum>(stat)] = value;
	}
	PossibleType GetStats(StatsEnum stat) const
	{
		return stats[*magic_enum::enum_index<StatsEnum>(stat)];
	}
	template <typename StatsType>
	StatsType GetStats(StatsEnum stat) const
	{
		PossibleType res = GetStats(stat);
		if (!IsInitialized(res))
			LOG(ERROR) << "Cannot find stat " << magic_enum::enum_name<StatsEnum>(stat) << ".";
		return std::get<StatsType>(GetStats(stat));
	}

	inline static StatsHolder<StatsEnum> GetDefaultStats(const std::string &base)
	{
		if (!defaultStats.contains(base)) {
			LOG(ERROR) << "Cannot find default " << magic_enum::enum_type_name<StatsEnum>() << " for " << base << ".";
		}
		return defaultStats.at(base);
	}

	inline static void FillStatsFromJson(nlohmann::json &info, const std::string &base, const std::string &typeName,
										 bool override)
	{
		if (typeName != base) {
			// Use base as basis
			defaultStats[base] = defaultStats[typeName];
		}
		for (nlohmann::json::iterator stat = info.begin(); stat != info.end(); ++stat) {
			auto casted = magic_enum::enum_cast<StatsEnum>(stat.key());
			if (!casted.has_value()) {
				// skip objects
				continue;
			}
			PossibleType value;
			if (stat.value().is_boolean())
				value = stat.value().get<bool>();
			else if (stat.value().is_number())
				value = stat.value().get<double>();
			else
				LOG(WARNING) << "Unexpected value type for " << stat.key() << ".";

			if (!IsInitialized(defaultStats[base].GetStats(casted.value())) || override)
				defaultStats[base].SetStraightStats(casted.value(), value);
		}
		LOG(INFO) << "Successfully extracted default " << typeName << " stats for " << base << '.';

		if (typeName == base) {
			FillUnitialized(base);
		}
	}

	inline static void ClearDefaults()
	{
		defaultStats.clear();
	}

  private:
	inline static void FillUnitialized(const std::string &base)
	{
		for (auto &statHolder : defaultStats)
			if (statHolder.first != base)
				for (auto &eStat : magic_enum::enum_values<StatsEnum>()) {
					const auto &stat = statHolder.second.GetStats(eStat);
					if (!IsInitialized(stat)) {
						const auto &baseStat = defaultStats.at(base).GetStats(eStat);
						statHolder.second.SetStraightStats(eStat, baseStat);
					}
				}
	}

	/**
	 * @brief Set stat directly, uncontrollable
	 *
	 * @param stat
	 * @param value
	 */
	void SetStraightStats(StatsEnum stat, PossibleType value)
	{
		stats[*magic_enum::enum_index<StatsEnum>(stat)] = value;
	}
	inline static std::map<std::string, StatsHolder<StatsEnum>> defaultStats;

	std::vector<PossibleType> stats;
};

struct make_string_functor {
	std::string operator()(bool &x) const
	{
		return x ? "True" : "False";
	}
	std::string operator()(double x) const
	{
		return std::to_string(x);
	}
	std::string operator()(std::monostate) const
	{
		return "NoType";
	}
};
