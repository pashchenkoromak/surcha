#pragma once
#include <math.h>
#include <string>
/**
 * @brief Default precision for doubles checks
 */
const double EPS = 1e-4;

/**
 * @brief Check if two double are close to each other (true, if the difference is not more than eps)
 *
 * @param a
 * @param b
 * @param eps - precision, used to check
 * @return true if numbers are close
 */
bool isClose(double a, double b, double eps = EPS);

/**
 * @brief Return a sqr of a number; (a^2)
 *
 * @param a
 * @return a * a
 */
double sqr(double a);

template <typename T>
struct TPoint;

using Point = TPoint<double>;
using IPoint = TPoint<int>;

/**
 * @brief A point, storing 2 values as x and y.
 */
template <typename T>
struct TPoint {
	/**
	 * @brief Coords
	 */
	T x;
	T y;

	/**
	 * @brief Construct a new TPoint from TPoint of other type (int to double and vv).
	 *
	 * @tparam T1
	 * @param rhs
	 */
	template <typename T1>
	TPoint(TPoint<T1> rhs)
	{
		x = (T)rhs.x;
		y = (T)rhs.y;
	}
	/**
	 * @brief Construct a new Point object with given coords.
	 *
	 * @param _x
	 * @param _y
	 */
	TPoint(T _x, T _y);
	/**
	 * @brief Construct a new Point object with (0,0) coords.
	 */
	TPoint();
	/**
	 * @brief Return distance from point to (0,0).
	 * @return double
	 */
	double length() const;

	/**
	 * @brief Return point which is difference between two points, as (5,5)-(1,2)=(4,3).
	 *
	 * @param a
	 * @param b
	 * @return TPoint
	 */
	friend TPoint operator-(TPoint a, TPoint b)
	{
		return TPoint(a.x - b.x, a.y - b.y);
	}
	/**
	 * @brief Same as minus, but plus.
	 *
	 * @param a
	 * @param b
	 * @return TPoint
	 */
	friend TPoint operator+(TPoint a, TPoint b)
	{
		return TPoint(a.x + b.x, a.y + b.y);
	}
	/**
	 * @brief Divides every coord of point to the number (double or int), as (10, 15)/5=(2,3)
	 *
	 * @tparam T1
	 * @param a
	 * @param b
	 * @return TPoint
	 */
	template <typename T1>
	friend TPoint operator/(TPoint a, T1 b)
	{
		return TPoint(a.x / b, a.y / b);
	}
	/**
	 * @brief Same as division, but multiplification
	 *
	 * @tparam T1
	 * @param a
	 * @param b
	 * @return TPoint
	 */
	template <typename T1>
	friend TPoint operator*(TPoint a, T1 b)
	{
		return TPoint(a.x * b, a.y * b);
	}
	/**
	 * @brief Outputs Point (converted to string) to the stream
	 *
	 * @param output
	 * @param p
	 * @return std::ostream&
	 */
	friend std::ostream &operator<<(std::ostream &output, const TPoint &p)
	{
		output << (std::string)p;
		return output;
	}
	/**
	 * @brief Checks if Point is less than other.
	 * Note: Check x coord, then (if x are equals) - y coord.
	 * So: (1,1) < (2,3)
	 * (1,1)<(2,0)
	 * (1,1)<(1,2)
	 * @param rhs
	 * @return true if less
	 */
	bool operator<(const TPoint &rhs) const;
	/**
	 * @brief Check if both coords are the same
	 *
	 * @param rhs
	 * @return true
	 * @return false
	 */
	bool operator==(const TPoint &rhs) const;
	/**
	 * @brief Check if at least one coord is different
	 *
	 * @param rhs
	 * @return true
	 * @return false
	 */
	bool operator!=(const TPoint &rhs) const;

	operator std::string() const
	{
		return "(" + std::to_string(x) + ", " + std::to_string(y) + ")";
	}
	/**
	 * @brief Get the closest to this point with integer coords.
	 * Note: 0.5 -> 1, 0.4 -> 0.
	 * So: (0.5,0.1)->(1,0)
	 * @return Point
	 */
	IPoint GetClosestIntPoint() const;
	/**
	 * @brief check if other point is close to its (with eps accuracy).
	 *
	 * @param rhs
	 * @param eps - by default is EPS
	 * @return true
	 */
	bool IsClose(TPoint rhs, double eps = EPS) const
	{
		return isClose(x, rhs.x, eps) && isClose(y, rhs.y, eps);
	}
};

/**
 * @brief Calculate distance between two Points
 *
 * @tparam T
 * @param a
 * @param b
 * @return double
 */
template <typename T>
double Distance(TPoint<T> a, TPoint<T> b)
{
	return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

/**
 * @brief Helper for moving points
 *
 * @param from - starting point
 * @param to - destination point
 * @param speed
 * @param dt
 * @return Return the position of point 'from' after dt time of going in 'to' direction with 'speed' speed.
 */
Point MovePoint(Point from, Point to, const double speed, const double dt);
