// surcha.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "MainManager.h"
#include "UnitManager.h"
#include "FrameStorage.h"
#include <chrono>
#include <thread>
#include <glog/logging.h>
#include <iomanip>

/*void CustomPrefix(std::ostream &s, const LogMessageInfo &l, void *)
{
	using std::setfill;
	using std::setw;

	s << l.severity[0] << setw(2) << l.time.min() << ':' << setw(2) << l.time.sec() << "." << setw(6) <<
l.time.usec()
	  << ' ' << setfill(' ') << setw(5) << l.thread_id << setfill('0') << ' ' << l.filename << ':' <<
l.line_number
	  << "]";
}
*/
bool GameOver()
{
	auto mainBases = UnitManager::Instance().GetUnits(
		[](std::shared_ptr<BaseUnit> unit) { return unit->GetUnitType() == eUnits::BaseMainStructure; });
	return mainBases.size() <= 1;
}

int main(int argc, char *argv[])
{
	// Initialize Google’s logging library.
	google::InitGoogleLogging(argv[0]);

	const int mills = 15;
	const double dt = 0.001 * mills;
	MainManager::Instance().Init(2);
	graphics::FrameStorage first_state;
	MainManager::Instance().Update(dt, first_state, MainManager::Instance().Graphics->frame_buffer.writer_event_buffer);
	auto t = std::thread(&graphics::BaseConcurrentGE::DrawLoop, MainManager::Instance().Graphics.get(), first_state);
	// for (int i = 0; i < 200; i++) {
	int i = 0;
	while (!GameOver()) {
		LOG_EVERY_N(INFO, 10) << "Frame num " << i << '.';
		MainManager::Instance().Update(dt, MainManager::Instance().Graphics->frame_buffer.ReplaceWritable(),
									   MainManager::Instance().Graphics->frame_buffer.writer_event_buffer);
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
		i++;
	}
	MainManager::Instance().Graphics->setTerminationFlag(true);
	t.join();
}
