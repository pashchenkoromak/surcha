#pragma once

#include "FrameStorage.h"
#include "BaseGE.h"

namespace graphics
{
class ConsoleConcurrentGE : public BaseConcurrentGE
{
  public:
	ConsoleConcurrentGE();

	/// <summary>
	/// Return singleton instance
	/// </summary>
	static ConsoleConcurrentGE &Instance();

	virtual void DrawFrame(const FrameStorage &data) override;

  private:
	static const std::map<Terrain, char> Terrains;
	static const std::map<eUnits, char> Units;
	static const std::map<eProjectives, char> Projectives;
	std::vector<std::vector<char>> field;

	ConsoleConcurrentGE(const ConsoleConcurrentGE &) = delete;
};

} // namespace graphics