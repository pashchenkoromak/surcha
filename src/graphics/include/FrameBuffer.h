
#pragma once
#include <mutex>
#include <unordered_set>

namespace graphics
{
template <typename DataFrame>
class FrameBuffer
{
  public:
	FrameBuffer() : drawing_id(2), filling_id(0), ready_id(1)
	{
	}
	FrameBuffer(const DataFrame &df) : drawing_id(2), filling_id(0), ready_id(1)
	{
		dataframes.fill(df);
	}
	void fill(const DataFrame &df)
	{
		m.lock();
		dataframes.fill(df);
		m.unlock();
	}

	/**
	 * @brief Request a pointer to a new writable object. This makes the old writeable object unsafe to write.
	 * @return a pointer to a new and only writeable object.
	 */
	DataFrame &ReplaceWritable()
	{
		m.lock();
		ReplaceWritableNonBlocking();
		m.unlock();
		return dataframes[filling_id];
	}
	/**
	 * @brief Request a pointer to a new readable object. This makes the old readable object unsafe to read.
	 * @return a reference to a new and only readable object.
	 */
	DataFrame &ReplaceReadable()
	{
		if (drawing_id != ready_id) {
			m.lock();
			drawing_id = ready_id;
			m.unlock();
		}
		return dataframes[drawing_id];
	}

  protected:
	inline void ReplaceWritableNonBlocking()
	{
		if (drawing_id == ready_id) {
			ready_id = filling_id;
			filling_id = (ready_id + 1) % 3;
			if (filling_id == drawing_id) {
				filling_id = (ready_id + 2) % 3;
			}
		} else {
			std::swap(ready_id, filling_id);
		}
	}
	inline void ReplaceREadableNonBlocking()
	{
		drawing_id = ready_id;
	}

	std::array<DataFrame, 3> dataframes;
	int drawing_id, filling_id, ready_id;
	std::mutex m;
};

template <typename DataFrame>
class FrameBufferWithEvents : public FrameBuffer<DataFrame>
{
  public:
	FrameBufferWithEvents() : FrameBuffer<DataFrame>(), writer_event_buffer(), reader_event_buffer()
	{
	}
	FrameBufferWithEvents(const DataFrame &df)
		: FrameBuffer<DataFrame>(df), writer_event_buffer(), reader_event_buffer()
	{
	}

	/**
	 * @brief Extends ReplaceWritables by moving the collected Events to be accessible to the reader, not the
	 * writer.
	 * @return a pointer to a new and only writeable object.
	 */
	DataFrame &ReplaceWritableMoveEvents()
	{
		this->m.lock();
		this->ReplaceWritableNonBlocking();
		writer_event_buffer.merge(reader_event_buffer);
		this->m.unlock();
		return this->dataframes[this->filling_id];
	}
	/**
	 * @brief Extends ReplaceReadable with events.
	 * @return a reference to a new and only readable object.
	 */
	DataFrame &ReplaceReadableReadEvents(std::unordered_multiset<std::unique_ptr<BaseEvent>> &destination_event_set)
	{
		if (this->drawing_id != this->ready_id) {
			this->m.lock();
			this->drawing_id = this->ready_id;
			destination_event_set.merge(reader_event_buffer);
			this->m.unlock();
		}
		return this->dataframes[this->drawing_id];
	}
	/**
	 * @brief the buffer where the writer can write events. For now, the
	 *
	 */
	std::unordered_multiset<std::unique_ptr<BaseEvent>> writer_event_buffer;

  protected:
	std::unordered_multiset<std::unique_ptr<BaseEvent>> reader_event_buffer;
};

} // namespace graphics