#pragma once

#include "BaseUnit.h"
#include "BaseProjective.h"
#include "Terrain.h"

#include <set>
#include <memory>
#include <atomic>
#include <vector>

namespace graphics
{
/**
 * @brief Used to store data for
 * @todo make sure this always has a deep copy constructor!
 */
struct FrameStorage {
	FrameStorage() = default;
	double time = 0;
	std::set<std::shared_ptr<BaseUnit>> units;
	std::set<std::shared_ptr<BaseProjective>> shots;
	std::vector<std::vector<Terrain>> terrain;
};
} // namespace graphics