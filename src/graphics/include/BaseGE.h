#pragma once
#include <vector>
#include <map>
#include <memory>
#include <mutex>

#include "FrameStorage.h"
#include "Events.h"

#include "FrameBuffer.h"

namespace graphics
{
class BaseGE
{
  public:
	virtual void DrawFrame(const FrameStorage &data) = 0;
};

class BaseConcurrentGE : protected BaseGE
{
  public:
	virtual void DrawFrame(const FrameStorage &data) = 0;
	/**
	 * @brief The function to be used with std::thread. Continuously draws frames from the buffer.
	 * @param first_frame This value is used to initialized every dataframe in framebuffer.
	 */
	void DrawLoop(const FrameStorage &first_frame);
	/**
	 * @brief Set the Termination flag
	 * @param value If set to True, the DrawConcurrently() method will terminate on the beginning of the next cycle.
	 */
	void setTerminationFlag(bool value);
	/**
	 * @brief frame buffer. Logics can use request_writable to get a writeablle data frame.
	 * @todo Should I make it private and create a separate function in BaseConcurrentGE?
	 */
	FrameBufferWithEvents<FrameStorage> frame_buffer;

  protected:
	std::unordered_multiset<std::unique_ptr<BaseEvent>> events;
	BaseConcurrentGE() : frame_buffer(), events(), termination_flag(false), readable_dataframe(nullptr)
	{
	}

  private:
	/**
	 * @brief is true when the termination of the DrawConcurrently method is requested, false otherwise
	 */
	std::atomic<bool> termination_flag;
	/**
	 * @brief pointer to a currently readable dataframe;
	 *
	 */
	FrameStorage *readable_dataframe;

	BaseConcurrentGE(const BaseConcurrentGE &) = delete;
};

} // namespace graphics