

#include <string>
#include <memory>
#include <functional>

namespace graphics
{
enum EventUpdateReturnValue { REMOVE_WITHOUT_RENDER, RENDER_AND_REMOVE, RENDER };
class BaseEvent
{
  public:
	/**
	 * @brief Get the name of the event
	 *
	 * @return std::string
	 */
	virtual std::string getName();
	// here loads of virtual getter functions for all possible event parameters.

	/**
	 * @brief
	 *
	 * @param ctx current FrameStorage, used to get information, that is required to render the effect (for ex. the
	 * subject of "burning")
	 * @return EventUpdateReturnValue, pretty self-explanatory
	 */
	virtual EventUpdateReturnValue Update(const FrameStorage &ctx) = 0;
	virtual ~BaseEvent(){};

  protected:
	const std::string name;
	BaseEvent(const std::string name) : name(name){};

  private:
	// const int hash;
	// static std::atomic<int> hash_counter;
};
// std::atomic<int> BaseEvent::hash_counter{ 0 };

/**
 * @brief this even has customizable name and will be shown for {life_time} seconds
 * @todo move method definitions to .cpp file
 */
class SimpleTimedEvent : public BaseEvent
{
  public:
	SimpleTimedEvent(const std::string &name, double start_time, double life_time)
		: BaseEvent(name), end_time(start_time + life_time){};

	virtual EventUpdateReturnValue Update(const FrameStorage &ctx) override
	{
		if (ctx.time > end_time) {
			return REMOVE_WITHOUT_RENDER;
		} else {
			return RENDER;
		}
	};
	virtual ~SimpleTimedEvent() = default;

  protected:
	double end_time;
};

} // namespace graphics