#include "BaseGE.h"
namespace graphics
{
void BaseConcurrentGE::DrawLoop(const FrameStorage &first_frame)
{
	frame_buffer.fill(first_frame);
	while (!termination_flag.load(std::memory_order_acquire)) {
		readable_dataframe = &frame_buffer.ReplaceReadableReadEvents(this->events);
		DrawFrame(*readable_dataframe);
	}
}

void BaseConcurrentGE::setTerminationFlag(bool value)
{
	termination_flag.store(value, std::memory_order_release);
}

} // namespace graphics