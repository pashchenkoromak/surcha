#include "ConsoleConcurrentGE.h"
#include "Stats.h"
#include "GeometryHelper.h"
#include <iostream>
#include <thread>

namespace graphics
{
const std::map<Terrain, char> ConsoleConcurrentGE::Terrains = { { Terrain::NONE, ' ' },
																{ Terrain::FIELD, '_' },
																{ Terrain::ROAD, '.' } };

const std::map<eUnits, char> ConsoleConcurrentGE::Units = { { eUnits::BaseUnit, 'b' },
															{ eUnits::BaseArcher, 'a' },
															{ eUnits::BaseMainStructure, 'm' } };
const std::map<eProjectives, char> ConsoleConcurrentGE::Projectives = { { eProjectives::BaseProjective, '*' } };

ConsoleConcurrentGE::ConsoleConcurrentGE()
{
}

void ConsoleConcurrentGE::DrawFrame(const FrameStorage &data)
{
	const size_t N = data.terrain.size();
	field.resize(N);
	for (size_t i = 0; i < N; i++) {
		const size_t M = data.terrain[i].size();
		field[i].resize(M);

		for (size_t j = 0; j < M; j++)
			field[i][j] = Terrains.at(data.terrain[i][j]);
	}

	for (auto &unit : data.units) {
		Point pos = unit->GetPos();
		if (pos.x < field.size() && pos.y < field[0].size() && pos.x >= 0 && pos.y >= 0)
			field[pos.x][pos.y] = Units.at(unit->GetUnitType());
	}
	for (auto &proj : data.shots) {
		Point pos = proj->GetPos();
		if (pos.x < field.size() && pos.y < field[0].size())
			field[pos.x][pos.y] = Projectives.at(proj->GetProjectiveType());
	}

	for (auto &row : field) {
		for (auto &el : row)
			std::cout << el;
		std::cout << std::endl;
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
}

} // namespace graphics