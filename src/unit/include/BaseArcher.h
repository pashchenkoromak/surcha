#pragma once
#include "BaseUnit.h"
#include <memory>
#include <iostream>

enum class ArcherStats { range, firing_time, projective_speed };

/**
 * @brief class BaseArcher is a base class for any archery units and structures.
 * In Update() it looks for any enemies nearby and tries to shot into them.
 * If there are no enemies - move to the enemy's base.
 */
class BaseArcher : public BaseUnit
{
  public:
	using BaseUnit::GetStats;
	using BaseUnit::OnSetStats;
	using BaseUnit::SetStats;
	using IPhysicalObject::GetStats;
	using IPhysicalObject::OnSetStats;
	using IPhysicalObject::SetStats;
	void SetStats(ArcherStats e, PossibleType value);
	void OnSetStats(ArcherStats e, PossibleType oldValue, PossibleType value);
	template <typename StatsType>
	StatsType GetStats(ArcherStats e) const
	{
		return stats.GetStats<StatsType>(e);
	}
	virtual eUnits GetUnitType() const override;

	/**
	 * @brief Construct a new Base Archer object
	 * Initialize it with constants.
	 * TODO: move constants into the file.
	 */
	BaseArcher();

	/**
	 * @brief Update current state of unit.
	 * @param dt
	 */
	virtual void Update(const double dt) override;

  protected:
	/**
	 * @brief Check if there is an enemy nearby.
	 * @param enemy - ptr to the enemy or nullptr
	 * @return true if there is an enemy
	 */
	virtual bool NeedFire(std::shared_ptr<BaseUnit> enemy);

	/**
	 * @brief Make a shot to the enemy
	 * @param target - enemy
	 */
	virtual void Fire(std::shared_ptr<BaseUnit> target);

	/**
	 * @brief Making a shot. It takes fireTime, and when it expires - produce a projective.
	 * @param dt
	 */
	virtual void ProcessFire(const double dt);

	/**
	 * @brief If it is false - don't call Move() on updates.
	 */
	virtual bool CanWalk() const
	{
		bool movable = GetStats<bool>(PhysicalObjectStats::movable);
		bool speed = GetStats<double>(PhysicalObjectStats::speed);
		return movable && !isClose(speed, .0);
	}
	virtual void Init(const std::string &base);

  private:
	StatsHolder<ArcherStats> stats, baseStats;

	/**
	 * @brief Get the nearest enemy in the archer's attack range
	 * @return std::shared_ptr<BaseUnit> of an enemy
	 * @return nullptr if there no enemies in range
	 */
	std::shared_ptr<BaseUnit> GetNearestEnemy() const;

	/**
	 * @brief Stored target to shoot into.
	 */
	std::shared_ptr<BaseUnit> target;

	/**
	 * @brief True - if is in process of firing
	 */
	bool firing;

	/**
	 * @brief Left time to the shot
	 */
	double fireTimeLeft;
};
