class BaseUnit;
#pragma once
#include "GeometryHelper.h"
#include "IPhysicalObject.h"
#include <memory>

enum class UnitStats { damage, vision_range, attack_cooldown, hit_points };

class BaseUnit : public IPhysicalObject
{
  public:
	using IPhysicalObject::GetStats;
	using IPhysicalObject::OnSetStats;
	using IPhysicalObject::SetStats;
	void SetStats(UnitStats e, PossibleType value);
	void OnSetStats(UnitStats e, PossibleType oldValue, PossibleType value);
	template <typename StatsType>
	StatsType GetStats(UnitStats e) const
	{
		return stats.GetStats<StatsType>(e);
	}
	virtual eUnits GetUnitType() const override;
	virtual void Init(const std::string &base);

	virtual void Update(const double dt);

	virtual void DealDamage(double damage) override;

	virtual Point OnCollision(std::shared_ptr<IPhysicalObject> object, const Point &collisionLocation) override;

	int GetPlayerId() const;
	bool IsNeutral() const;
	virtual bool CanFight();
	virtual void Init(int _playerId);
	virtual void SetDestination(Point _destination);
	virtual void Move(Point where, const double dt);

	bool operator<(const BaseUnit &rhs) const;

	BaseUnit();
	BaseUnit(const BaseUnit &) = default;
	BaseUnit(BaseUnit &&) = default;
	BaseUnit &operator=(BaseUnit &&) = default;
	BaseUnit &operator=(const BaseUnit &) = default;

  protected:
	int playerId;
	Point destination;
	StatsHolder<UnitStats> stats, baseStats;
};
