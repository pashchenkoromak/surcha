#pragma once
#include "BaseUnit.h"
#include "Stats.h"
#include "BaseArcher.h"

enum class MainStructureStats { spawn_cooldown };

class BaseMainStructure : public BaseArcher
{
  public:
	using BaseArcher::GetStats;
	using BaseArcher::OnSetStats;
	using BaseArcher::SetStats;
	using BaseUnit::GetStats;
	using BaseUnit::OnSetStats;
	using BaseUnit::SetStats;
	using IPhysicalObject::GetStats;
	using IPhysicalObject::OnSetStats;
	using IPhysicalObject::SetStats;
	void SetStats(MainStructureStats e, PossibleType value);
	void OnSetStats(MainStructureStats e, PossibleType oldValue, PossibleType value);
	template <typename StatsType>
	StatsType GetStats(MainStructureStats e) const
	{
		return stats.GetStats<StatsType>(e);
	}
	virtual eUnits GetUnitType() const override;

	BaseMainStructure();
	virtual void Update(const double dt) override;

	virtual void Init(const std::string &base);

  protected:
	void Spawn();

  private:
	double spawnCdLeft;
	StatsHolder<MainStructureStats> stats, baseStats;
};
