class IPhysicalObject;

#pragma once
#include "GeometryHelper.h"
#include "ILogicalEffect.h"
#include "Stats.h"
#include <memory>
#include <set>
#include <variant>

enum class PhysicalObjectStats { speed, size, mass, direction, movable };
/**
 * @brief Aliases
 *
 */
constexpr PhysicalObjectStats s_speed = PhysicalObjectStats::speed;
constexpr PhysicalObjectStats s_size = PhysicalObjectStats::size;
constexpr PhysicalObjectStats s_mass = PhysicalObjectStats::mass;
constexpr PhysicalObjectStats s_direction = PhysicalObjectStats::direction;
constexpr PhysicalObjectStats s_movable = PhysicalObjectStats::movable;

class IPhysicalObject : public std::enable_shared_from_this<IPhysicalObject>
{
  public:
	/**
	 * @brief These methods are used only when use them outside of class
	 * @begin
	 */
	template <typename Derived, typename StatsEnum>
	void SetStats(StatsEnum e, PossibleType value)
	{
		PossibleType oldValue = GetStats<Derived, StatsEnum, PossibleType>(e);
		shared_from_base<Derived>()->SetStats(e, value);
		OnSetStats<Derived, StatsEnum>(e, oldValue, value);
	}
	template <typename Derived, typename StatsEnum, typename StatsType>
	StatsType GetStats(StatsEnum e) const
	{
		return shared_from_base<Derived>()->GetStats(e);
	}
	template <typename Derived, typename StatsEnum>
	int OnSetStats(StatsEnum e, PossibleType oldValue, PossibleType newValue)
	{
		return shared_from_base<Derived>()->OnSetStats(e, oldValue, newValue);
	}
	/* @end */

	void SetStats(PhysicalObjectStats e, PossibleType value);
	void OnSetStats(PhysicalObjectStats e, PossibleType oldValue, PossibleType value);
	template <typename StatsType>
	StatsType GetStats(PhysicalObjectStats e) const
	{
		return stats.GetStats<StatsType>(e);
	}
	virtual eUnits GetUnitType() const;
	virtual eProjectives GetProjectiveType() const;

	/**
	 * @brief Initialize PhisicalStats with default values, taken from "base" unit.
	 *
	 * @param base
	 */
	virtual void Init(const std::string &base);

	virtual Point GetPos() const;
	virtual void SetPos(const Point &newPos);
	virtual Point OnCollision(std::shared_ptr<IPhysicalObject> object, const Point &collisionLocation)
	{
		(void)object;
		(void)collisionLocation;
		return {};
	}

	virtual const std::string &GetName() const;
	id_t GetId() const;
	virtual std::string nameid() const;
	virtual void DealDamage(double damage);

	virtual void AddEffect(std::shared_ptr<ILogicalEffect>);
	virtual void RemoveEffect(std::shared_ptr<ILogicalEffect>);

	IPhysicalObject();

  protected:
	template <typename Derived>
	std::shared_ptr<Derived> shared_from_base()
	{
		return std::static_pointer_cast<Derived>(shared_from_this());
	}

	std::set<std::shared_ptr<ILogicalEffect>> appliedEffects;
	std::string objectName;

	Point pos;
	StatsHolder<PhysicalObjectStats> stats, baseStats;

	id_t id;
	static id_t max_id;
};
