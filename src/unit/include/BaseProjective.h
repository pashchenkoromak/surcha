#pragma once
#include "IPhysicalObject.h"
#include "BaseUnit.h"
#include "Stats.h"
#include <memory>

enum class ProjectiveStats { damage };

class BaseProjective : public IPhysicalObject
{
  public:
	using IPhysicalObject::GetStats;
	using IPhysicalObject::OnSetStats;
	using IPhysicalObject::SetStats;
	void SetStats(ProjectiveStats e, PossibleType value);
	void OnSetStats(ProjectiveStats e, PossibleType oldValue, PossibleType value);
	template <typename StatsType>
	StatsType GetStats(ProjectiveStats e) const
	{
		return stats.GetStats<StatsType>(e);
	}

	virtual void Init(const std::string &base);
	BaseProjective();
	virtual void Init(std::shared_ptr<IPhysicalObject> _target, std::shared_ptr<IPhysicalObject> dealer, double speed,
					  double _damage);
	virtual void Update(const double dt);
	virtual eProjectives GetProjectiveType() const;

	/// <summary>
	/// Check, if shot is ready to be destroyed.
	/// </summary>
	/// <returns></returns>
	virtual bool Expired() const;
	virtual Point OnCollision(std::shared_ptr<IPhysicalObject> object, const Point &collisionLocation) override;

  private:
	double direction;
	bool expired;
	std::shared_ptr<IPhysicalObject> target;
	StatsHolder<ProjectiveStats> stats, baseStats;
};
