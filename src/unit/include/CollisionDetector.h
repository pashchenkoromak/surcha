#pragma once
#include "UnitManager.h"
#include "IPhysicalObject.h"
#include <map>
#include <set>

typedef std::set<std::shared_ptr<IPhysicalObject>> physobj_set;
typedef std::map<id_t, physobj_set> cached_collisions_t;

class CollisionDetector
{
  public:
	static CollisionDetector &Instance();
	void Update(const double dt);
	void FixCollisions(physobj_set &units) const;
	void FixCollisions(physobj_set &units, const cached_collisions_t &usedCachedCollisions) const;
	cached_collisions_t MakeCachedCollisions(const physobj_set &units, const double dt) const;

  private:
	physobj_set GetCollisions(std::shared_ptr<IPhysicalObject> object, Point collisionPoint,
							  const cached_collisions_t &usedCollisions) const;

	CollisionDetector() = default;
	cached_collisions_t cachedCollisions;
};
