#include "BaseMainStructure.h"
#include "UnitManager.h"
#include "BaseArcher.h"
#include "MainManager.h"
#include <glog/logging.h>

BaseMainStructure::BaseMainStructure() : spawnCdLeft(0)
{
	objectName = "BaseMainStructure";
}

void BaseMainStructure::Update(const double dt)
{
	if (spawnCdLeft <= dt) {
		spawnCdLeft = GetStats<double>(MainStructureStats::spawn_cooldown);
		Spawn();
	} else {
		spawnCdLeft -= dt;
	}
	BaseArcher::Update(dt);
}

void BaseMainStructure::Spawn()
{
	LOG(INFO) << nameid() << " spawn units.";
	std::shared_ptr<BaseUnit> archer = std::make_shared<BaseArcher>();
	archer->SetPos({ pos.x + 1, pos.y + 1 });
	archer->Init(playerId);
	archer->Init("BaseArcher");
	auto enemyBases = MainManager::Instance().GetPlayer(playerId).GetEnemyBases();
	archer->SetDestination(*enemyBases.begin());
	UnitManager::Instance().AddUnit(archer);
}

void BaseMainStructure::SetStats(MainStructureStats e, PossibleType v)
{
	stats.SetStats(e, v);
}
void BaseMainStructure::OnSetStats(MainStructureStats e, PossibleType oldValue, PossibleType value)
{
	VLOG(1) << "Stat " << magic_enum::enum_name<MainStructureStats>(e) << " was changed from "
			<< std::visit(make_string_functor(), oldValue) << " to " << std::visit(make_string_functor(), value) << ".";
}

eUnits BaseMainStructure::GetUnitType() const
{
	return eUnits::BaseMainStructure;
}

void BaseMainStructure::Init(const std::string &base)
{
	baseStats = StatsHolder<MainStructureStats>::GetDefaultStats(base);
	stats = baseStats;
	BaseArcher::Init(base);
}
