#include "BaseProjective.h"
#include "GeometryHelper.h"
#include "UnitManager.h"
#include "MainManager.h"
#include "Stats.h"
#include <glog/logging.h>
#include <string>

BaseProjective::BaseProjective() : expired(false)
{
	objectName = "BaseProjective";
}

void BaseProjective::Init(std::shared_ptr<IPhysicalObject> _target, std::shared_ptr<IPhysicalObject> dealer,
						  double _speed, double _damage)
{
	target = _target;
	SetStats(s_speed, _speed);
	expired = false;
	SetStats(ProjectiveStats::damage, _damage);
	pos = dealer->GetPos();
	LOG(INFO) << nameid() << " was initialized.";
}

void BaseProjective::Update(const double dt)
{
	auto newPos = MovePoint(pos, target->GetPos(), GetStats<double>(s_speed), dt);

	VLOG(2) << nameid() << " is moving from " << pos << " to " << newPos << ".";
	pos = newPos;
	auto dist = (pos - target->GetPos()).length();
	if (dist < EPS) {
		LOG(INFO) << nameid() << " shoot into " << target->nameid() << ".";
		UnitManager::Instance().DoDamage(target, GetStats<double>(ProjectiveStats::damage));
		expired = true;
	}
}

bool BaseProjective::Expired() const
{
	return expired;
}

Point BaseProjective::OnCollision(std::shared_ptr<IPhysicalObject> object, const Point &collisionLocation)
{
	VLOG(2) << nameid() << " shoot into " << object->nameid() << " at " << collisionLocation << ".";
	return {};
}

eProjectives BaseProjective::GetProjectiveType() const
{
	return eProjectives::BaseProjective;
}
void BaseProjective::SetStats(ProjectiveStats e, PossibleType v)
{
	stats.SetStats(e, v);
}
void BaseProjective::OnSetStats(ProjectiveStats e, PossibleType oldValue, PossibleType value)
{
	VLOG(1) << "Stat " << magic_enum::enum_name<ProjectiveStats>(e) << " was changed from "
			<< std::visit(make_string_functor(), oldValue) << " to " << std::visit(make_string_functor(), value) << ".";
}

void BaseProjective::Init(const std::string &base)
{
	baseStats = StatsHolder<ProjectiveStats>::GetDefaultStats(base);
	stats = baseStats;
	IPhysicalObject::Init(base);
}