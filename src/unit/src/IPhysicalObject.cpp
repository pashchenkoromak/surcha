#include "IPhysicalObject.h"
#include <glog/logging.h>

id_t IPhysicalObject::max_id = 0;

id_t IPhysicalObject::GetId() const
{
	return id;
}

IPhysicalObject::IPhysicalObject() : id(max_id++)
{
}

Point IPhysicalObject::GetPos() const
{
	return pos;
}
void IPhysicalObject::SetPos(const Point &newPos)
{
	VLOG(1) << nameid() << " moved from " << pos << " to " << newPos << ".";
	pos = newPos;
}
const std::string &IPhysicalObject::GetName() const
{
	return objectName;
}

void IPhysicalObject::DealDamage(double damage)
{
	(void)damage;
}

std::string IPhysicalObject::nameid() const
{
	static const std::string nameId = GetName() + " " + std::to_string(GetId());

	return nameId;
}

void IPhysicalObject::AddEffect(std::shared_ptr<ILogicalEffect> effect)
{
	appliedEffects.insert(effect);
}
void IPhysicalObject::RemoveEffect(std::shared_ptr<ILogicalEffect> effect)
{
	if (appliedEffects.contains(effect))
		appliedEffects.erase(effect);
}

void IPhysicalObject::SetStats(PhysicalObjectStats e, PossibleType v)
{
	stats.SetStats(e, v);
}
void IPhysicalObject::OnSetStats(PhysicalObjectStats e, PossibleType oldValue, PossibleType value)
{
	VLOG(1) << "Stat " << magic_enum::enum_name<PhysicalObjectStats>(e) << " was changed from "
			<< std::visit(make_string_functor(), oldValue) << " to " << std::visit(make_string_functor(), value) << ".";
}

eUnits IPhysicalObject::GetUnitType() const
{
	return eUnits::PhysicalObject;
}
eProjectives IPhysicalObject::GetProjectiveType() const
{
	return eProjectives::PhysicalObject;
}

void IPhysicalObject::Init(const std::string &base)
{
	baseStats = StatsHolder<PhysicalObjectStats>::GetDefaultStats(base);
	stats = baseStats;
}