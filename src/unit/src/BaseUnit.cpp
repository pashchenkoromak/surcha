#include "BaseUnit.h"
#include "CollisionDetector.h"
#include "PathFinder.h"
#include "MainManager.h"
#include <glog/logging.h>

void BaseUnit::Init(int _playerId)
{
	LOG(INFO) << nameid() << " initialized.";
	playerId = _playerId;
}

void BaseUnit::Move(Point where, const double dt)
{
	where = PathFinder::Instance().GetNextStep(GetPos(), where);
	double speed = GetStats<double>(s_speed);
	auto newPos = MovePoint(GetPos(), where, speed, dt);
	SetPos(newPos);
}

bool BaseUnit::operator<(const BaseUnit &rhs) const
{
	return GetId() < rhs.GetId();
}

void BaseUnit::Update(const double dt)
{
	(void)dt;
}

int BaseUnit::GetPlayerId() const
{
	return playerId;
}
bool BaseUnit::IsNeutral() const
{
	return playerId != -1;
}

void BaseUnit::SetDestination(Point _destination)
{
	destination = _destination;
}

Point BaseUnit::OnCollision(std::shared_ptr<IPhysicalObject> object, const Point &collisionLocation)
{
	bool movable = GetStats<bool>(s_movable);
	if (!movable)
		return {};

	bool isObjectUnit = object->GetUnitType() != eUnits::PhysicalObject;
	if (!isObjectUnit)
		return {};

	VLOG(2) << nameid() << " collides with " << object->nameid() << " in " << collisionLocation;
	Point vec = collisionLocation - object->GetPos();
	if (vec.length() < EPS) {
		vec = { EPS, EPS };
	}

	double object_size = object->GetStats<double>(s_size), size = GetStats<double>(s_size), dist = vec.length();

	Point newPos;
	if (!object->GetStats<bool>(s_movable)) {
		auto coef = (object_size + size) / dist + EPS;
		newPos = (object->GetPos() + vec * coef);
	} else {
		const double mass = GetStats<double>(s_mass);
		const double object_mass = object->GetStats<double>(s_mass);

		double move2 = (object_size + size - dist) / (mass / object_mass + 1);
		newPos = object->GetPos() + vec * (dist + move2 + EPS) / dist;
	}

	VLOG(2) << nameid() << " has to move to " << newPos;
	return newPos - collisionLocation;
}

BaseUnit::BaseUnit()
{
	objectName = "BaseUnit";
}

bool BaseUnit::CanFight()
{
	return GetStats<double>(UnitStats::hit_points) > 0;
}

void BaseUnit::SetStats(UnitStats e, PossibleType v)
{
	stats.SetStats(e, v);
}
void BaseUnit::OnSetStats(UnitStats e, PossibleType oldValue, PossibleType value)
{
	VLOG(1) << "Stat " << magic_enum::enum_name<UnitStats>(e) << " was changed from "
			<< std::visit(make_string_functor(), oldValue) << " to " << std::visit(make_string_functor(), value) << ".";
}

eUnits BaseUnit::GetUnitType() const
{
	return eUnits::BaseUnit;
}

void BaseUnit::DealDamage(double damage)
{
	SetStats(UnitStats::hit_points, GetStats<double>(UnitStats::hit_points) - damage);
}

void BaseUnit::Init(const std::string &base)
{
	baseStats = StatsHolder<UnitStats>::GetDefaultStats(base);
	stats = baseStats;
	IPhysicalObject::Init(base);
}
