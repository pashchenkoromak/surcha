#include "CollisionDetector.h"
#include <vector>
#include <algorithm>

CollisionDetector &CollisionDetector::Instance()
{
	static CollisionDetector cd;
	return cd;
}

void CollisionDetector::FixCollisions(std::set<std::shared_ptr<IPhysicalObject>> &units) const
{
	FixCollisions(units, cachedCollisions);
}

void CollisionDetector::FixCollisions(
	std::set<std::shared_ptr<IPhysicalObject>> &units,
	const std::map<id_t, std::set<std::shared_ptr<IPhysicalObject>>> &usedCachedCollisions) const
{
	std::map<std::shared_ptr<IPhysicalObject>, Point> shift;
	for (auto &unit : units) {
		std::set<std::shared_ptr<IPhysicalObject>> collisions;
		if (shift.find(unit) != shift.end())
			collisions = GetCollisions(unit, unit->GetPos() + shift.at(unit), usedCachedCollisions);
		else
			collisions = GetCollisions(unit, unit->GetPos(), usedCachedCollisions);
		if (collisions.empty())
			continue;

		for (auto &collision : collisions) {
			shift[unit] = shift[unit] + unit->OnCollision(collision, unit->GetPos() + shift[unit]);
		}
	}

	auto iter = shift.begin();
	auto endIter = shift.end();
	for (; iter != endIter;) {
		if (iter->second == Point()) {
			iter = shift.erase(iter);
		} else {
			++iter;
		}
	}

	if (shift.empty())
		return;
	for (auto &unit : units) {
		unit->SetPos(unit->GetPos() + shift[unit]);
	}
	FixCollisions(units, usedCachedCollisions);
}

physobj_set CollisionDetector::GetCollisions(std::shared_ptr<IPhysicalObject> object, Point collisionPoint,
											 const cached_collisions_t &usedCollisions) const
{
	if (usedCollisions.find(object->GetId()) == usedCollisions.end())
		return {};

	physobj_set res;
	auto possibleCollisions = usedCollisions.at(object->GetId());
	for (auto &candidate : possibleCollisions) {
		if (Distance(collisionPoint, candidate->GetPos()) + EPS <=
			candidate->GetStats<double>(s_size) + object->GetStats<double>(s_size))
			res.insert(candidate);
	}
	return res;
}

void CollisionDetector::Update(const double dt)
{
	cachedCollisions = MakeCachedCollisions(UnitManager::Instance().GetAllPhysicalObjects(), dt);
}

std::map<id_t, std::set<std::shared_ptr<IPhysicalObject>>>
CollisionDetector::MakeCachedCollisions(const std::set<std::shared_ptr<IPhysicalObject>> &objects,
										const double dt) const
{
	std::map<id_t, std::set<std::shared_ptr<IPhysicalObject>>> res;

	for (auto object : objects) {
		std::set<std::shared_ptr<IPhysicalObject>> closeObjects;
		for (auto candidate : objects) {
			double possibleRange = candidate->GetStats<double>(s_size) + object->GetStats<double>(s_size) +
								   dt * (candidate->GetStats<double>(s_speed) + object->GetStats<double>(s_speed));
			if (Distance(object->GetPos(), candidate->GetPos()) <= possibleRange &&
				object->GetId() != candidate->GetId())
				closeObjects.insert(candidate);
		}
		res[object->GetId()] = std::move(closeObjects);
	}
	return res;
}