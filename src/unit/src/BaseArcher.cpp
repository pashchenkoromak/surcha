#include "BaseArcher.h"
#include <UnitManager.h>
#include <MainManager.h>
#include "BaseProjective.h"
#include <glog/logging.h>

BaseArcher::BaseArcher()
{
	fireTimeLeft = 0;
	firing = false;
	objectName = "BaseArcher";
}

void BaseArcher::Update(const double dt)
{
	if (firing) {
		ProcessFire(dt);
		return;
	}
	auto nearestEnemy = GetNearestEnemy();
	if (!NeedFire(nearestEnemy)) {
		if (CanWalk())
			Move(destination, dt);
	} else {
		Fire(nearestEnemy);
		ProcessFire(dt);
	}
}

bool BaseArcher::NeedFire(std::shared_ptr<BaseUnit> enemy)
{
	return enemy != nullptr;
}

void BaseArcher::Fire(std::shared_ptr<BaseUnit> _target)
{
	LOG(INFO) << nameid() << " fire.";
	target = _target;
	firing = true;
	fireTimeLeft = GetStats<double>(ArcherStats::firing_time);
}

void BaseArcher::ProcessFire(const double dt)
{
	if (fireTimeLeft <= 0) {
		auto shot = std::make_shared<BaseProjective>(BaseProjective());
		shot->Init((std::string)magic_enum::enum_name<eProjectives>(eProjectives::BaseProjective));
		shot->Init(target, shared_from_base<BaseArcher>(), GetStats<double>(ArcherStats::projective_speed),
				   GetStats<double>(UnitStats::damage));
		LOG(INFO) << nameid() << " created a projective " << shot->nameid();
		UnitManager::Instance().AddShot(shot);
		fireTimeLeft = GetStats<double>(ArcherStats::firing_time);
		firing = false;
	} else {
		fireTimeLeft -= dt;

		LOG(INFO) << nameid() << " is firing. " << fireTimeLeft << " left.";
	}
}

std::shared_ptr<BaseUnit> BaseArcher::GetNearestEnemy() const
{
	auto closeEnemyUnits = UnitManager::Instance().GetUnits([this](std::shared_ptr<BaseUnit> unit) {
		return unit->GetPlayerId() != playerId && Distance(unit->GetPos(), pos) <= GetStats<double>(ArcherStats::range);
	});
	if (!closeEnemyUnits.empty())
		return *closeEnemyUnits.begin();
	else
		return nullptr;
}

void BaseArcher::SetStats(ArcherStats e, PossibleType v)
{
	stats.SetStats(e, v);
}
void BaseArcher::OnSetStats(ArcherStats e, PossibleType oldValue, PossibleType value)
{
	VLOG(1) << "Stat " << magic_enum::enum_name<ArcherStats>(e) << " was changed from "
			<< std::visit(make_string_functor(), oldValue) << " to " << std::visit(make_string_functor(), value) << ".";
}

eUnits BaseArcher::GetUnitType() const
{
	return eUnits::BaseArcher;
}

void BaseArcher::Init(const std::string &base)
{
	baseStats = StatsHolder<ArcherStats>::GetDefaultStats(base);
	stats = baseStats;
	BaseUnit::Init(base);
}