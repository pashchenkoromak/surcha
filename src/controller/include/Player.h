#pragma once
#include "GeometryHelper.h"
#include <vector>
class Player
{
  public:
	Player(int _id);
	void Init(Point mainPos);
	int GetId() const;
	void Update(const double dt);
	const std::vector<Point> &GetEnemyBases() const
	{
		return enemyBases;
	}
	bool operator<(const Player &rhs) const
	{
		return id < rhs.id;
	}

  private:
	int id;
	Point mainPos;
	std::vector<Point> enemyBases;
};
