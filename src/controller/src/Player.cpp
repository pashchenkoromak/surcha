#include "Player.h"
#include "Map.h"
#include "UnitManager.h"
#include "BaseMainStructure.h"

Player::Player(int _id) : id(_id), mainPos(-1, -1)
{
}

void Player::Init(Point _mainPos)
{
	mainPos = _mainPos;
	enemyBases = Map::Instance().GetPlayerBasePoints();
	for (auto it = enemyBases.begin(); it != enemyBases.end(); it++)
		if (*it == mainPos) {
			enemyBases.erase(it);
			break;
		}
	std::shared_ptr<BaseUnit> mainStruct = std::make_shared<BaseMainStructure>();
	mainStruct->Init(id);
	mainStruct->Init("BaseMainStructure");
	mainStruct->SetPos(_mainPos);
	UnitManager::Instance().AddUnit(mainStruct);
}

int Player::GetId() const
{
	return id;
}

void Player::Update(const double dt)
{
	(void)dt;
}
