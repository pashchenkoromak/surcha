#pragma once
#include <map>

enum class Terrain { FIELD, ROAD, NONE };
const std::map<char, Terrain> Char2Terrain{ { '_', Terrain::FIELD }, { '.', Terrain::ROAD }, { '-', Terrain::NONE } };