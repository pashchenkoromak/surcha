#pragma once
#include "GeometryHelper.h"

class PathFinder
{
  public:
	static PathFinder &Instance();
	Point GetNextStep(Point currentPos, Point Destination) const;

  private:
	PathFinder() = default;
};
