#pragma once
#include "GeometryHelper.h"
#include "Terrain.h"
#include <string>
#include <memory>
#include <set>
#include <vector>

/// <summary>
/// Map utils, Singleton
/// </summary>
class Map
{
  public:
	/// <summary>
	/// Return singleton instance
	/// </summary>
	/// <returns></returns>
	static Map &Instance();

	size_t GetPlayersCount() const;
	std::vector<Point> GetPlayerBasePoints() const;

	Terrain At(const Point) const;
	Terrain At(const int x, const int y) const;
	void Update(const double dt);

	const std::vector<std::vector<Terrain>> &GetField() const;
	void SetField(std::vector<std::vector<Terrain>> newField);
	void SetPlayerBases(const std::vector<Point> &bases);
	std::vector<IPoint> GetPath(IPoint from, IPoint to) const;
	void SetPassable(IPoint p);
	void SetUnpassable(IPoint p);
	void CalculatePathes();

  private:
	void CalculatePath(IPoint from);
	void UpdatePath(IPoint from, IPoint prevPos, IPoint dir, std::map<IPoint, double> &dist,
					std::vector<IPoint> &queue);
	bool isPassable(IPoint p) const;
	bool IsPointInField(IPoint p) const;

	/// <summary>
	/// Current field state.
	/// </summary>
	std::vector<std::vector<Terrain>> field;
	std::map<std::pair<IPoint, IPoint>, std::vector<IPoint>> pathes;
	std::set<IPoint> unpassables;
	std::vector<Point> bases;
};
