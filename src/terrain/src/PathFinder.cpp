#include "PathFinder.h"
#include <vector>
#include "Map.h"
#include <glog/logging.h>

PathFinder &PathFinder::Instance()
{
	static PathFinder pf;
	return pf;
}

Point PathFinder::GetNextStep(Point currentPos, Point Destination) const
{
	IPoint iCurrentPos = currentPos.GetClosestIntPoint();
	IPoint iDestPos = Destination.GetClosestIntPoint();
	VLOG(2) << "Get next step from " << iCurrentPos << " to " << iDestPos;
	if (iCurrentPos == iDestPos)
		return iDestPos;
	return Map::Instance().GetPath(iCurrentPos, iDestPos)[1];
}
