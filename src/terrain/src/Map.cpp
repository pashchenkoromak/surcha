#include "Map.h"
#include <fstream>
#include <iostream>
#include <glog/logging.h>

Map &Map::Instance()
{
	static Map instance;
	return instance;
}

size_t Map::GetPlayersCount() const
{
	return bases.size();
}

void Map::SetPlayerBases(const std::vector<Point> &_bases)
{
	bases = _bases;
	for (auto &base : bases)
		SetUnpassable(base);
}

std::vector<Point> Map::GetPlayerBasePoints() const
{
	return bases;
}

Terrain Map::At(const Point pos) const
{
	return At((int)pos.x, (int)pos.y);
}

Terrain Map::At(const int x, const int y) const
{
	return field[x][y];
}

void Map::Update(const double dt)
{
	(void)dt;
}

const std::vector<std::vector<Terrain>> &Map::GetField() const
{
	return field;
}

void Map::SetField(std::vector<std::vector<Terrain>> newField)
{
	field = newField;
}

/**
 * @brief First Point in vector is Point from
 *
 * @param from
 * @param to
 * @return std::vector<Point>
 */
std::vector<IPoint> Map::GetPath(IPoint from, IPoint to) const
{
	auto pair = std::make_pair(from, to);
	if (pathes.find(pair) == pathes.end())
		return {};
	return pathes.at(pair);
}

std::vector<IPoint> GetDirections()
{
	std::vector<int> dir_coord{ -1, 0, 1 };
	std::vector<IPoint> dirs;
	dirs.reserve(8);
	for (int x : dir_coord)
		for (int y : dir_coord)
			if (x != 0 || y != 0)
				dirs.push_back({ x, y });
	return dirs;
}

void Map::UpdatePath(IPoint from, IPoint prevPos, IPoint dir, std::map<IPoint, double> &dist,
					 std::vector<IPoint> &queue)
{
	IPoint newPoint = prevPos + dir;
	auto pair = std::make_pair(from, newPoint);
	if (!isPassable(newPoint)) {
		pathes[pair] = pathes[std::make_pair(from, prevPos)];
		pathes[pair].push_back(newPoint);
	} else {
		double newDist = dist[prevPos] + dir.length();
		if (!dist.contains(newPoint) || dist.at(newPoint) > newDist) {
			dist[newPoint] = newDist;
			queue.push_back(newPoint);
			pathes[pair] = pathes[std::make_pair(from, prevPos)];
			pathes[pair].push_back(newPoint);
		}
	}
}

void Map::CalculatePath(IPoint from)
{
	if (!isPassable(from))
		return;

	auto dirs = GetDirections();
	std::vector<IPoint> queue;
	queue.reserve(field.size() * field[0].size());
	std::map<IPoint, double> dist;

	queue.push_back(from);
	dist[from] = 0;

	size_t tail = 0;
	while (tail < queue.size()) {
		IPoint pos = queue[tail];
		for (auto dir : dirs) {
			if (!IsPointInField(pos + dir))
				continue;
			UpdatePath(from, pos, dir, dist, queue);
		}
		tail++;
	}
}

void Map::CalculatePathes()
{
	LOG(INFO) << "Calculate all possible pathes.";
	if (field.size() == 0)
		return;
	const size_t n = field.size(), m = field[0].size();
	for (size_t x1 = 0; x1 < n; x1++) {
		VLOG(1) << "Calculated all pathes for " << x1 << " line.";
		for (size_t y1 = 0; y1 < m; y1++) {
			CalculatePath(IPoint(x1, y1));
			VLOG(2) << "Calculated all pathes for " << IPoint(x1, y1) << '.';
		}
	}
}

bool ValueInBounds(int value, std::pair<int, int> bounds)
{
	return value >= bounds.first && value <= bounds.second;
}

bool Map::IsPointInField(IPoint p) const
{
	static std::pair<int, int> map_bounds_x = std::make_pair(0, field.size() - 1),
							   map_bounds_y = std::make_pair(0, field[0].size() - 1);
	return ValueInBounds(p.x, map_bounds_x) && ValueInBounds(p.y, map_bounds_y);
}

bool Map::isPassable(IPoint p) const
{
	return IsPointInField(p) && field[p.x][p.y] != Terrain::NONE && unpassables.find(p) == unpassables.end();
}

void Map::SetPassable(IPoint p)
{
	unpassables.erase(p);
	LOG(INFO) << "Position " << p << " become passable.";
}
void Map::SetUnpassable(IPoint p)
{
	unpassables.insert(p);
	LOG(INFO) << "Position " << p << " become unpassable.";
}
