#include "EffectManager.h"
#include <glog/logging.h>

EffectManager &EffectManager::Instance()
{
	static EffectManager mm;
	return mm;
}

void EffectManager::Update(const double dt)
{
	for (auto &effect : effects) {
		effect->Update(dt);
		if (effect->IsFinished())
			effect->OnFinish();
	}
}

void EffectManager::AddEffect(std::shared_ptr<ILogicalEffect> effect)
{
	effects.insert(effect);
	effect->OnStart();
}
