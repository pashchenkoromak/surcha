#include "UnitManager.h"
#include <nlohmann/json.hpp>
#include <glog/logging.h>
#include "CollisionDetector.h"
#include <fstream>

UnitManager &UnitManager::Instance()
{
	static UnitManager um;
	return um;
}

void UnitManager::UpdateShots(const double dt)
{
	auto shot_it = shots.begin();
	while (shot_it != shots.end()) {
		auto current = shot_it++;
		if (current->get()->Expired()) {
			LOG(INFO) << current->get()->nameid() << " was destroyed.";
			shots.erase(current);
		} else
			current->get()->Update(dt);
	}
}
void UnitManager::UpdateUnits(const double dt)
{
	auto unit_it = units.begin();
	while (unit_it != units.end()) {
		auto current = unit_it++;
		if (!current->get()->CanFight()) {
			LOG(INFO) << current->get()->nameid() << " was destroyed.";
			units.erase(current);
		} else
			current->get()->Update(dt);
	}
}

void UnitManager::Update(const double dt)
{
	UpdateUnits(dt);
	UpdateShots(dt);
	FixCollisions();
}

void UnitManager::FixCollisions()
{
	std::set<std::shared_ptr<IPhysicalObject>> obj;
	for (auto &unit : units)
		obj.insert(unit);
	CollisionDetector::Instance().FixCollisions(obj);
}

void UnitManager::AddUnit(std::shared_ptr<BaseUnit> newUnit)
{
	units.insert(newUnit);
	LOG(INFO) << "New " << newUnit->nameid() << " was created.";
}

void UnitManager::AddShot(std::shared_ptr<BaseProjective> newShot)
{
	shots.insert(newShot);
	LOG(INFO) << "New " << newShot->nameid() << " was created.";
}

void UnitManager::DoDamage(std::shared_ptr<IPhysicalObject> target, double damage)
{
	target->DealDamage(damage);
}
std::set<std::shared_ptr<BaseUnit>> UnitManager::GetAllUnits() const
{
	std::set<std::shared_ptr<BaseUnit>> res;
	for (auto &unit : units)
		res.insert(unit);
	return res;
}
std::set<std::shared_ptr<BaseProjective>> UnitManager::GetAllShots() const
{
	std::set<std::shared_ptr<BaseProjective>> res;
	for (auto &shot : shots)
		res.insert(shot);
	return res;
}

std::set<std::shared_ptr<IPhysicalObject>> UnitManager::GetAllPhysicalObjects() const
{
	std::set<std::shared_ptr<IPhysicalObject>> res;
	for (std::shared_ptr<IPhysicalObject> unit : units) {
		res.insert(unit);
	}
	for (std::shared_ptr<IPhysicalObject> shot : shots) {
		res.insert(shot);
	}
	return res;
}