#include "MainManager.h"
#include "UnitManager.h"
#include "CollisionDetector.h"
#include "ConsoleConcurrentGE.h"
#include "EffectManager.h"

#include <glog/logging.h>

MainManager::MainManager() : Graphics(std::make_unique<graphics::ConsoleConcurrentGE>())
{
}

MainManager &MainManager::Instance()
{
	static MainManager mm;
	return mm;
}

void MainManager::Init(size_t playerNum)
{
	dim.Init("maps/map.json");
	size_t playerCnt = std::min(playerNum, Map::Instance().GetPlayersCount());
	auto playerBases = Map::Instance().GetPlayerBasePoints();
	auto base = playerBases.begin();
	for (size_t i = 0; i < playerCnt && base != playerBases.end(); i++, base++) {
		LOG(INFO) << "Add player with id = " << i << ".";
		Player p(i);
		p.Init(*base);
		players.insert(p);
	}
	Map::Instance().CalculatePathes();
}

void MainManager::Draw(graphics::FrameStorage &graphics_data)
{
	graphics_data.shots = std::move(UnitManager::Instance().GetAllShots());
	graphics_data.units = std::move(UnitManager::Instance().GetAllUnits());
	graphics_data.terrain = Map::Instance().GetField();
	LOG(INFO) << "Draw information updated.";
}

void MainManager::Update(const double dt, graphics::FrameStorage &graphics_data,
						 std::unordered_multiset<std::unique_ptr<graphics::BaseEvent>> &event_set)
{
	Map::Instance().Update(dt);
	CollisionDetector::Instance().Update(dt);
	UnitManager::Instance().Update(dt);
	EffectManager::Instance().Update(dt);
	for (auto player : players) {
		player.Update(dt);
	}
	Draw(graphics_data);
}

const Player &MainManager::GetPlayer(const int id)
{
	return *players.find(id);
}

DefaultInfoManager &MainManager::GetDefaultInfoManager()
{
	return dim;
}