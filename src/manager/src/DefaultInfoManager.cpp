#include "DefaultInfoManager.h"
#include "Terrain.h"
#include "Map.h"
#include "Stats.h"
#include <glog/logging.h>
#include <nlohmann/json.hpp>
#include <fstream>

#include "IPhysicalObject.h"
#include "BaseUnit.h"
#include "BaseArcher.h"
#include "BaseMainStructure.h"
#include "BaseProjective.h"

nlohmann::json GetJsonFromFile(const std::string &filename)
{
	nlohmann::json info;
	std::ifstream input(filename);
	input >> info;
	return info;
}

void DefaultInfoManager::Init(const std::string &map,
							  const std::string &defaultUnits /*= "settings/DefaultUnits.json"*/,
							  const std::string &defaultProjectives /*= "settings/DefaultProjectives.json"*/)
{
	nlohmann::json infoUnits = GetJsonFromFile(defaultUnits);
	nlohmann::json infoProj = GetJsonFromFile(defaultProjectives);
	nlohmann::json infoMap = GetJsonFromFile(map);
	Init(infoMap, infoUnits, infoProj);
}

void FillStatsFromJson(nlohmann::json &object, const std::string &base, const std::string &typeName, bool override)
{
	if (typeName == "PhysicalObject")
		StatsHolder<PhysicalObjectStats>::FillStatsFromJson(object, base, typeName, override);
	if (typeName == "BaseUnit")
		StatsHolder<UnitStats>::FillStatsFromJson(object, base, typeName, override);
	if (typeName == "BaseArcher")
		StatsHolder<ArcherStats>::FillStatsFromJson(object, base, typeName, override);
	if (typeName == "BaseMainStructure")
		StatsHolder<MainStructureStats>::FillStatsFromJson(object, base, typeName, override);
	if (typeName == "BaseProjective")
		StatsHolder<ProjectiveStats>::FillStatsFromJson(object, base, typeName, override);
}

template <typename EnumName>
void FillDefaults(nlohmann::json &objects, bool override = false, const std::string_view &base = "")
{
	for (nlohmann::json::iterator object = objects.begin(); object != objects.end(); ++object) {
		auto casted = magic_enum::enum_cast<EnumName>(object.key());
		if (!casted.has_value()) {
			// Skip non objects
			continue;
		}
		std::string typeName = (std::string)object.key();
		std::string tmpBase = (base == "" ? typeName : (std::string)base);
		if (object->is_object()) {
			FillStatsFromJson(object.value(), tmpBase, typeName, override);
			FillDefaults<EnumName>(object.value(), override, tmpBase);
		}
	}
}

void DefaultInfoManager::Init(nlohmann::json &map, nlohmann::json &units, nlohmann::json &projectives)
{
	FillDefaults<eUnits>(units, false);
	FillDefaults<eProjectives>(projectives, false);
	InitMap(map);
}

void DefaultInfoManager::InitMap(nlohmann::json &info)
{
	if (info.contains("map")) {
		std::vector<std::vector<Terrain>> field;
		int j = 0;
		for (auto &line : info["map"]) {
			field.push_back({});
			for (char &h : line.get<std::string>()) {
				field[j].emplace_back(Char2Terrain.at(h));
			}
			j++;
		}
		Map::Instance().SetField(field);
	}
	if (info.contains("player_bases")) {
		std::vector<Point> bases;
		for (auto &point : info["player_bases"])
			bases.push_back({ point[0], point[1] });
		Map::Instance().SetPlayerBases(bases);
	}

	if (info.contains("units")) {
		FillDefaults<eUnits>(info["units"], true);
	}
	if (info.contains("projectives")) {
		FillDefaults<eProjectives>(info["projectives"], true);
	}
}