#pragma once
#include "BaseUnit.h"
#include "BaseProjective.h"
#include <set>
#include <string>
#include <memory>
#include <map>

class UnitManager
{
  public:
	/// <summary>
	/// Return singleton instance
	/// </summary>
	/// <returns></returns>
	static UnitManager &Instance();

	/// <summary>
	/// Updates units and shots for dt.
	/// </summary>
	/// <param name="dt"></param>
	void Update(const double dt);
	void AddUnit(std::shared_ptr<BaseUnit> newUnit);
	void AddShot(std::shared_ptr<BaseProjective> newShot);
	void DoDamage(std::shared_ptr<IPhysicalObject> target, double damage);

	std::set<std::shared_ptr<BaseUnit>> GetAllUnits() const;
	std::set<std::shared_ptr<BaseProjective>> GetAllShots() const;
	std::set<std::shared_ptr<IPhysicalObject>> GetAllPhysicalObjects() const;

	template <typename F>
	std::set<std::shared_ptr<IPhysicalObject>> GetObjects(F &&lambda)
	{
		std::set<std::shared_ptr<IPhysicalObject>> res;
		for (auto unit : units) {
			if (lambda(unit))
				res.insert(unit);
		}
		for (auto shot : shots) {
			if (lambda(shot))
				res.insert(shot);
		}
		return std::move(res);
	}

	template <typename F>
	std::set<std::shared_ptr<BaseUnit>> GetUnits(F &&lambda)
	{
		std::set<std::shared_ptr<BaseUnit>> res;
		for (auto unit : units) {
			if (lambda(unit))
				res.insert(unit);
		}
		return res;
	}

	template <typename F>
	std::set<std::shared_ptr<BaseProjective>> GetShots(F &&lambda)
	{
		std::set<std::shared_ptr<BaseProjective>> res;
		for (auto shot : shots) {
			if (lambda(shot))
				res.insert(shot);
		}
		return res;
	}

  private:
	UnitManager() = default;
	void UpdateUnits(const double dt);
	void UpdateShots(const double dt);
	void FixCollisions();
	std::set<std::shared_ptr<BaseUnit>> units;
	std::set<std::shared_ptr<BaseProjective>> shots;
	bool statsInited;
};
