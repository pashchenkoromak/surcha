#include <string>
#include "Stats.h"
#include <nlohmann/json.hpp>
#include "IPhysicalObject.h"

class DefaultInfoManager
{
  public:
	DefaultInfoManager() = default;
	void Init(const std::string &map, const std::string &defaultUnits = "settings/DefaultUnits.json",
			  const std::string &defaultProjectives = "settings/DefaultProjectives.json");
	void Init(nlohmann::json &map, nlohmann::json &units, nlohmann::json &projectives);

  private:
	std::set<std::shared_ptr<IPhysicalObject>> objects;
	void InitMap(nlohmann::json &mapInfo);
};