#pragma once
#include "Player.h"
#include "Map.h"
#include "DefaultInfoManager.h"
#include <BaseGE.h>
#include <memory>
#include <unordered_map>
/// <summary>
/// Knows everything about everyone.
/// </summary>
class MainManager
{
  public:
	/// <summary>
	/// Return singleton instance
	/// </summary>
	static MainManager &Instance();
	const std::unique_ptr<graphics::BaseConcurrentGE> Graphics;
	void Init(size_t playerNum);

	void Update(const double dt, graphics::FrameStorage &graphics_data,
				std::unordered_multiset<std::unique_ptr<graphics::BaseEvent>> &event_set);
	const Player &GetPlayer(const int id);
	DefaultInfoManager &GetDefaultInfoManager();
  private:
	MainManager();
	void Draw(graphics::FrameStorage &graphics_data);
	// todo
  private:
	DefaultInfoManager dim;
	std::set<Player> players;
	int filling_id;
};
