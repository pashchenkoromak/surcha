#pragma once
#include "ILogicalEffect.h"
#include <set>

/// <summary>
/// Knows everything about everyone.
/// </summary>
class EffectManager
{
  public:
	/// <summary>
	/// Return singleton instance
	/// </summary>
	static EffectManager &Instance();
	void Update(const double dt);
	void AddEffect(std::shared_ptr<ILogicalEffect> effect);

  private:
	EffectManager() = default;

  private:
	std::set<std::shared_ptr<ILogicalEffect>> effects;
};
