#include "ILogicalEffect.h"

/**
 * @brief Effect changes speed of a group of units. The more units - the lower effect.
 * @note In sum the strength of effect remain constant.
 *
 */
class SplittedHaste final : public ILogicalEffect
{
	SplittedHaste(double splittingAcceleration);
	void OnStart() override;
	void OnFinish() override;
	void OnStartAffecting(std::shared_ptr<IPhysicalObject>) override;
	void OnFinishAffecting(std::shared_ptr<IPhysicalObject>) override;

  private:
	double baseAcceleration;
};
