class ILogicalEffect;
#pragma once

#include "IPhysicalObject.h"
#include <set>
#include <memory>

class ILogicalEffect : private std::enable_shared_from_this<ILogicalEffect>
{
  public:
	ILogicalEffect();
	virtual void Update(const double dt);

	virtual void OnStart();
	virtual void OnFinish();

	virtual void OnStartAffecting(std::shared_ptr<IPhysicalObject>) = 0;
	virtual void OnFinishAffecting(std::shared_ptr<IPhysicalObject>) = 0;

	virtual void AddAffected(std::shared_ptr<IPhysicalObject>);
	virtual void StopAffecting(std::shared_ptr<IPhysicalObject>);

	virtual void SetSource(std::shared_ptr<IPhysicalObject>);
	virtual double GetLeftTime() const;
	virtual bool IsPersistent() const;
	virtual bool IsFinished() const;

	virtual const std::string &GetName() const;
	id_t GetId() const;
	virtual const std::string &nameid() const;

	virtual std::shared_ptr<ILogicalEffect> getPtr();
	bool operator<(const ILogicalEffect &rhs) const;

  protected:
	std::set<std::shared_ptr<IPhysicalObject>> affected;
	std::shared_ptr<IPhysicalObject> source;
	std::string effectName;
	double timeLeft;
	const id_t id;
	static id_t max_id;
	const double priority;
};
