#include "SplittedHaste.h"

SplittedHaste::SplittedHaste(double splittingAcceleration) : baseAcceleration(splittingAcceleration)
{
	effectName = "Splitted haste";
}

void SplittedHaste::OnStartAffecting(std::shared_ptr<IPhysicalObject> object)
{
	const size_t N = affected.size();
	double eachHaste = baseAcceleration / N;
	if (N == 1) {
		object->SetStats(s_speed, object->GetStats<double>(s_speed) + eachHaste);
	} else {
		double previousHaste = baseAcceleration / (N - 1);
		for (auto &obj : affected) {
			object->SetStats(s_speed, object->GetStats<double>(s_speed) - previousHaste + eachHaste);
		}
	}
}

void SplittedHaste::OnFinishAffecting(std::shared_ptr<IPhysicalObject> object)
{
	const size_t N = affected.size();
	double previousHaste = baseAcceleration / (N + 1);
	double eachHaste = baseAcceleration / N;
	for (auto &obj : affected) {
		object->SetStats(s_speed, object->GetStats<double>(s_speed) - previousHaste + eachHaste);
	}
	object->SetStats(s_speed, object->GetStats<double>(s_speed) - previousHaste);
}
