#include "ILogicalEffect.h"

#include <glog/logging.h>

ILogicalEffect::ILogicalEffect() : id(max_id++), effectName("ILogicalEffect"), priority(0)
{
}

void ILogicalEffect::Update(const double dt)
{
	timeLeft -= dt;
}

double ILogicalEffect::GetLeftTime() const
{
	return timeLeft;
}

bool ILogicalEffect::IsPersistent() const
{
	return false;
}

bool ILogicalEffect::IsFinished() const
{
	return timeLeft <= 0;
}

void ILogicalEffect::OnStart()
{
	VLOG(1) << nameid() << " started.";
}

void ILogicalEffect::OnFinish()
{
	VLOG(1) << nameid() << " finished.";
}

void ILogicalEffect::AddAffected(std::shared_ptr<IPhysicalObject> object)
{
	if (!affected.contains(object)) {
		affected.insert(object);
		object->AddEffect(getPtr());
		OnStartAffecting(object);
	}
}

void ILogicalEffect::StopAffecting(std::shared_ptr<IPhysicalObject> object)
{
	if (affected.contains(object)) {
		affected.erase(object);
		object->RemoveEffect(getPtr());
		OnFinishAffecting(object);
	}
}
void ILogicalEffect::SetSource(std::shared_ptr<IPhysicalObject> object)
{
	source = object;
}

const std::string &ILogicalEffect::GetName() const
{
	return effectName;
}

id_t ILogicalEffect::GetId() const
{
	return id;
}

const std::string &ILogicalEffect::nameid() const
{
	static const std::string nameId = GetName() + std::to_string(GetId());
	return nameId;
}

std::shared_ptr<ILogicalEffect> ILogicalEffect::getPtr()
{
	return shared_from_this();
}

bool ILogicalEffect::operator<(const ILogicalEffect &rhs) const
{
	if (priority != rhs.priority)
		return priority > rhs.priority;
	return id > rhs.id;
}