#include <gtest/gtest.h>
#include "IPhysicalObject.h"
#include "BaseArcher.h"
#include "CollisionDetector.h"
#include "BaseMainStructure.h"

constexpr double dt = 0.015;

std::shared_ptr<IPhysicalObject> MakeArcher(Point pos, double size, double speed, double mass)
{
	std::shared_ptr<IPhysicalObject> archer = std::make_shared<BaseArcher>();
	archer->SetPos(pos);
	archer->SetStats(s_speed, speed);
	archer->SetStats(s_size, size);
	archer->SetStats(s_mass, mass);
	archer->SetStats(s_movable, true);
	return archer;
}

std::shared_ptr<IPhysicalObject> MakeStruct(Point pos, double size)
{
	std::shared_ptr<IPhysicalObject> base = std::make_shared<BaseMainStructure>();
	base->SetPos(pos);
	base->SetStats(s_speed, 0.);
	base->SetStats(s_size, size);
	base->SetStats(s_mass, 0.);
	base->SetStats(s_movable, false);
	return base;
}

void RunTest(std::vector<std::pair<std::shared_ptr<IPhysicalObject>, Point>> units_expect)
{
	std::set<std::shared_ptr<IPhysicalObject>> units;
	for (auto &unit_expect : units_expect) {
		units.insert(unit_expect.first);
	}
	auto cache = CollisionDetector::Instance().MakeCachedCollisions(units, dt);
	ASSERT_NO_THROW(CollisionDetector::Instance().FixCollisions(units, cache));

	for (auto &unit_expect : units_expect) {
		EXPECT_TRUE(unit_expect.first->GetPos().IsClose(unit_expect.second, 0.01));
	}
}

TEST(CollisionDetectorTest, two_units_no_collision)
{
	{
		std::vector<std::pair<std::shared_ptr<IPhysicalObject>, Point>> units_expect;

		std::shared_ptr<IPhysicalObject> a1 = MakeArcher({ 1, 1 }, 1, 1, 1);
		units_expect.push_back(std::make_pair(a1, Point(1, 1)));

		std::shared_ptr<IPhysicalObject> a2 = MakeArcher({ 1, 4 }, 1, 1, 1);
		units_expect.push_back(std::make_pair(a2, Point(1, 4)));

		RunTest(units_expect);
	}
}

TEST(CollisionDetectorTest, two_units_one_unmovable)
{
	std::vector<std::pair<std::shared_ptr<IPhysicalObject>, Point>> units_expect;

	std::shared_ptr<IPhysicalObject> a1 = MakeArcher({ 1, 1 }, 1, 1, 1);
	units_expect.push_back(std::make_pair(a1, Point(1, 0)));

	std::shared_ptr<IPhysicalObject> a2 = MakeStruct({ 1, 2 }, 1);
	units_expect.push_back(std::make_pair(a2, Point(1, 2)));

	RunTest(units_expect);
}

TEST(CollisionDetectorTest, three_units_no_collision)
{
	{
		std::vector<std::pair<std::shared_ptr<IPhysicalObject>, Point>> units_expect;

		std::shared_ptr<IPhysicalObject> a1 = MakeArcher({ 1, 1 }, 1, 1, 1);
		units_expect.push_back(std::make_pair(a1, Point(1, 1)));

		std::shared_ptr<IPhysicalObject> a2 = MakeArcher({ 1, 4 }, 1, 1, 1);
		units_expect.push_back(std::make_pair(a2, Point(1, 4)));

		std::shared_ptr<IPhysicalObject> a3 = MakeArcher({ 4, 1 }, 1, 1, 1);
		units_expect.push_back(std::make_pair(a3, Point(4, 1)));

		RunTest(units_expect);
	}
}

TEST(CollisionDetectorTest, two_units_collision)
{
	std::vector<std::pair<std::shared_ptr<IPhysicalObject>, Point>> units_expect;

	std::shared_ptr<IPhysicalObject> a1 = MakeArcher({ 1, 1 }, 1, 1, 1);
	units_expect.push_back(std::make_pair(a1, Point(1, 0.5)));

	std::shared_ptr<IPhysicalObject> a2 = MakeArcher({ 1, 2 }, 1, 1, 1);
	units_expect.push_back(std::make_pair(a2, Point(1, 2.5)));

	RunTest(units_expect);
}

TEST(CollisionDetectorTest, three_units_chain_collision)
{
	{
		std::vector<std::pair<std::shared_ptr<IPhysicalObject>, Point>> units_expect;

		std::shared_ptr<IPhysicalObject> a1 = MakeArcher({ 1, 1 }, 1, 1, 1);
		units_expect.push_back(std::make_pair(a1, Point(1, 0)));

		std::shared_ptr<IPhysicalObject> a2 = MakeArcher({ 1, 2 }, 1, 1, 1);
		units_expect.push_back(std::make_pair(a2, Point(1, 2)));

		std::shared_ptr<IPhysicalObject> a3 = MakeArcher({ 1, 3.5 }, 1, 1, 1);
		units_expect.push_back(std::make_pair(a3, Point(1, 4)));

		RunTest(units_expect);
	}
}