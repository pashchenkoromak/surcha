#include <gtest/gtest.h>
#include "IPhysicalObject.h"
#include "BaseArcher.h"
#include "CollisionDetector.h"
#include "BaseMainStructure.h"

void CheckStatsUpdate(std::shared_ptr<IPhysicalObject> obj, Point pos, double speed, double size, double mass,
					  double direction, bool movable)
{
	EXPECT_NO_THROW(obj->SetPos(pos));
	EXPECT_NO_THROW(obj->SetStats(s_speed, speed));
	EXPECT_NO_THROW(obj->SetStats(s_size, size));
	EXPECT_NO_THROW(obj->SetStats(s_mass, mass));
	EXPECT_NO_THROW(obj->SetStats(s_direction, direction));
	EXPECT_NO_THROW(obj->SetStats(s_movable, movable));
	EXPECT_TRUE(obj->GetPos().IsClose(pos));
	EXPECT_DOUBLE_EQ(obj->GetStats<double>(s_speed), speed);
	EXPECT_DOUBLE_EQ(obj->GetStats<double>(s_size), size);
	EXPECT_DOUBLE_EQ(obj->GetStats<double>(s_mass), mass);
	EXPECT_DOUBLE_EQ(obj->GetStats<double>(s_direction), direction);
	EXPECT_EQ(obj->GetStats<bool>(s_movable), movable);
}

TEST(StatsTest, PhysicalObject)
{
	std::shared_ptr<IPhysicalObject> obj = std::make_shared<IPhysicalObject>();
	CheckStatsUpdate(obj, { 1, 2 }, 1, 2, 3, 4, false);
	CheckStatsUpdate(obj, { 2, 1 }, 4, 3, 2, 1, true);
}
