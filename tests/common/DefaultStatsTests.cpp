#include "DefaultInfoManager.h"
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>
#include "Stats.h"
#include "BaseUnit.h"
#include "BaseMainStructure.h"

TEST(DefaultInfoManagerTest, base_unit_init)
{
	auto units = nlohmann::json::parse(R"( { "BaseUnit": { "damage": 1.0 } } )");
	auto proj = nlohmann::json::parse(R"( {} )");
	auto map = nlohmann::json::parse(R"( {} )");

	DefaultInfoManager dim;
	dim.Init(map, units, proj);
	auto stats = StatsHolder<UnitStats>::GetDefaultStats("BaseUnit");
	double damage = std::get<double>(stats.GetStats(UnitStats::damage));
	EXPECT_DOUBLE_EQ(damage, 1);
}

TEST(DefaultInfoManagerTest, base_unit_override)
{
	auto units = nlohmann::json::parse(R"( { "BaseUnit": { "damage": 1.0 } } )");
	auto proj = nlohmann::json::parse(R"( {} )");
	auto map = nlohmann::json::parse(R"( { "units" : { "BaseUnit": { "damage": 2.0 } } } )");

	DefaultInfoManager dim;
	StatsHolder<UnitStats>::ClearDefaults();
	dim.Init(map, units, proj);
	auto stats = StatsHolder<UnitStats>::GetDefaultStats("BaseUnit");
	double damage = std::get<double>(stats.GetStats(UnitStats::damage));
	EXPECT_DOUBLE_EQ(damage, 2);
}

TEST(DefaultInfoManagerTest, base_and_archer_init)
{
	auto units = nlohmann::json::parse(
		R"( { "BaseUnit": { "damage": 1.0 }, "BaseArcher": { "BaseUnit" : { "damage": 10.0 } } } )");
	auto proj = nlohmann::json::parse(R"( {} )");
	auto map = nlohmann::json::parse(R"( {} )");

	DefaultInfoManager dim;
	StatsHolder<UnitStats>::ClearDefaults();
	dim.Init(map, units, proj);
	{
		auto stats = StatsHolder<UnitStats>::GetDefaultStats("BaseUnit");
		double damage = std::get<double>(stats.GetStats(UnitStats::damage));
		EXPECT_DOUBLE_EQ(damage, 1);
	}
	{
		auto stats = StatsHolder<UnitStats>::GetDefaultStats("BaseArcher");
		double damage = std::get<double>(stats.GetStats(UnitStats::damage));
		EXPECT_DOUBLE_EQ(damage, 10);
	}
}

TEST(DefaultInfoManagerTest, base_and_archer_override_one)
{
	auto units = nlohmann::json::parse(
		R"( {
                "BaseUnit": {
                    "damage": 1.0
                },
                "BaseArcher": {
                    "BaseUnit" : {
                        "damage": 10.0
                    }
                }
            } )");
	auto proj = nlohmann::json::parse(R"( {} )");
	auto map = nlohmann::json::parse(R"(
        {
            "units" : {
                "BaseUnit": {
                    "damage": 2.0
                }
            }
        } )");

	DefaultInfoManager dim;
	StatsHolder<UnitStats>::ClearDefaults();
	dim.Init(map, units, proj);

	{
		auto stats = StatsHolder<UnitStats>::GetDefaultStats("BaseUnit");
		double damage = std::get<double>(stats.GetStats(UnitStats::damage));
		EXPECT_DOUBLE_EQ(damage, 2);
	}
	{
		auto stats = StatsHolder<UnitStats>::GetDefaultStats("BaseArcher");
		double damage = std::get<double>(stats.GetStats(UnitStats::damage));
		EXPECT_DOUBLE_EQ(damage, 10);
	}
}

TEST(DefaultInfoManagerTest, base_and_archer_override)
{
	auto units = nlohmann::json::parse(
		R"( {
                "BaseUnit": {
                    "damage": 1.0
                },
                "BaseArcher": {
                    "BaseUnit" : {
                        "damage": 10.0
                    }
                }
            } )");
	auto proj = nlohmann::json::parse(R"( {} )");
	auto map = nlohmann::json::parse(
		R"( {
            "units" : {
                "BaseUnit": {
                    "damage": 2.0
                },
                "BaseArcher": {
                    "BaseUnit": {
                        "damage": 20
                    }
                }
            }
        } )");
	DefaultInfoManager dim;
	StatsHolder<UnitStats>::ClearDefaults();
	dim.Init(map, units, proj);
	{
		auto stats = StatsHolder<UnitStats>::GetDefaultStats("BaseUnit");
		double damage = std::get<double>(stats.GetStats(UnitStats::damage));
		EXPECT_DOUBLE_EQ(damage, 2);
	}
	{
		auto stats = StatsHolder<UnitStats>::GetDefaultStats("BaseArcher");
		double damage = std::get<double>(stats.GetStats(UnitStats::damage));
		EXPECT_DOUBLE_EQ(damage, 20);
	}
}

TEST(DefaultInfoManagerTest, init_struct)
{
	auto units = nlohmann::json::parse(
		R"( {
                "BaseUnit": {
                    "damage": 1.0
                },
                "BaseArcher": {
                    "BaseUnit" : {
                        "damage": 10.0
                    }
                },
                "BaseMainStructure": {
                    "spawn_cooldown": 1,
                    "BaseUnit": {
                        "damage": 10
                    }
                }
            } )");
	auto proj = nlohmann::json::parse(R"( {} )");
	auto map = nlohmann::json::parse(R"( {} )");

	DefaultInfoManager dim;
	StatsHolder<UnitStats>::ClearDefaults();
	dim.Init(map, units, proj);

	{
		auto stats = StatsHolder<MainStructureStats>::GetDefaultStats("BaseMainStructure");
		double sc = std::get<double>(stats.GetStats(MainStructureStats::spawn_cooldown));
		EXPECT_DOUBLE_EQ(sc, 1);
	}
	{
		auto stats = StatsHolder<UnitStats>::GetDefaultStats("BaseMainStructure");
		double sc = std::get<double>(stats.GetStats(UnitStats::damage));
		EXPECT_DOUBLE_EQ(sc, 10);
	}
}