#include "GeometryHelper.h"
#include <gtest/gtest.h>

std::vector<double> test_vals{ -5, 0, 2.2 };
std::vector<double> coord{ -1, -0.5, 0, 0.5, 1 };
std::vector<Point> make_points()
{
	std::vector<Point> res;
	for (double val_x : coord)
		for (double val_y : coord)
			res.push_back(Point(val_x, val_y));
	return res;
}

TEST(PointTest, default_constructor)
{
	Point p;
	EXPECT_DOUBLE_EQ(p.x, 0);
	EXPECT_DOUBLE_EQ(p.y, 0);
}

TEST(PointTest, constructor_two_doubles)
{
	double a = 1, b = 5;
	Point p(a, b);
	EXPECT_DOUBLE_EQ(p.x, a);
	EXPECT_DOUBLE_EQ(p.y, b);
}

TEST(PointTest, lenght)
{
	for (Point p : make_points()) {
		double length = sqrt(sqr(p.x) + sqr(p.y));
		EXPECT_DOUBLE_EQ(p.length(), length);
	}
}
TEST(PointTest, point_minus_point)
{
	Point p1(3, 6);
	Point p2(1, 1);
	Point p3 = p2 - p1;
	EXPECT_DOUBLE_EQ(p3.x, -2);
	EXPECT_DOUBLE_EQ(p3.y, -5);
}
TEST(PointTest, point_plus_point)
{
	Point p1(10, 2);
	Point p2(4, -1);
	Point p3 = p1 + p2;
	EXPECT_DOUBLE_EQ(p3.x, 14);
	EXPECT_DOUBLE_EQ(p3.y, 1);
}
TEST(PointTest, divide_by_zero)
{
	Point p1(10, 2);
	int a = 0;
	EXPECT_NO_THROW(p1 / a);
}

TEST(PointTest, multiplication)
{
	Point p1(10, 2);
	for (double val : test_vals) {
		Point p2 = p1 * val;
		EXPECT_DOUBLE_EQ(p2.x, 10 * val);
		EXPECT_DOUBLE_EQ(p2.y, 2 * val);
	}
}
TEST(PointTest, divide)
{
	Point p1(10, 2);
	for (double val : test_vals) {
		Point p2 = p1 / val;
		EXPECT_DOUBLE_EQ(p2.x, 10 / val);
		EXPECT_DOUBLE_EQ(p2.y, 2 / val);
	}
}

TEST(PointTest, point_to_string)
{
	Point p1(-0.5, 2);
	EXPECT_EQ((std::string)p1, "(-0.500000, 2.000000)");
}
TEST(PointTest, ipoint_sum)
{
	TPoint<int> iP{ 4, 5 };
	IPoint iP2{ 1, -4 };
	EXPECT_EQ(iP + iP2, TPoint<int>(5, 1));
}

TEST(IPointTest, IPoint_default_construction)
{
	IPoint p;
	EXPECT_EQ(p.x, 0);
	EXPECT_EQ(p.y, 0);
}

TEST(IPointTest, IPoint_to_string)
{
	IPoint p1(-5, 0);
	EXPECT_EQ((std::string)p1, "(-5, 0)");
	IPoint p2(5, 0);
	EXPECT_EQ((std::string)p2, "(5, 0)");
	IPoint p3(-0, -0);
	EXPECT_EQ((std::string)p3, "(0, 0)");
	IPoint p4(-0.5, 0.2);
	EXPECT_EQ((std::string)p4, "(0, 0)");
	IPoint p5(-11.5, 1.75);
	EXPECT_EQ((std::string)p5, "(-11, 1)");
}

TEST(IPointTest, Point_to_IPoint)
{
	std::vector<std::pair<TPoint<double>, TPoint<int>>> tValues = {
		{ { 0, 0.4 }, { 0, 0 } }, { { 0, 0 }, { 0, 0 } },		{ { 0.5, 0.5 }, { 1, 1 } },
		{ { 1.1, 2 }, { 1, 2 } }, { { -0.5, 0.2 }, { -1, 0 } }, { { 1.5, 2.5 }, { 2, 3 } }
	};
	for (auto val : tValues) {
		EXPECT_EQ(val.first.GetClosestIntPoint(), val.second);
	}
}

TEST(TPointTest, Distance_between_two_Points)
{
	EXPECT_DOUBLE_EQ(Distance<double>({ 0, 0 }, { 0, 1 }), 1);
	EXPECT_DOUBLE_EQ(Distance<double>({ -1, -1 }, { 2, 3 }), sqrt(25));
	EXPECT_DOUBLE_EQ(Distance<double>({ 0, 0 }, { -2, -5 }), sqrt(29));
	EXPECT_DOUBLE_EQ(Distance<double>({ 0, 0 }, { -2.5, 5.1 }), sqrt(32.26));
}