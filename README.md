*******Conan setup*******

To install it just run:
    $ pip install conan

If you meet such a problem:

    ERROR: HTTPSConnectionPool(host='conan.bintray.com', port=443): Max retries exceeded with url: /v1/ping (Caused by SSLError(SSLCertVerificationError(1, '[SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: certificate has expired (_ssl.c:1131)')))

You may force conan ignore ssl certification by using:
    $ conan remote add conan-center https://conan.bintray.com False -f

**************************


********Building and running*********

    $ ./build.sh

Built executable is named surcha and located in bin/bin/ directory.

Logs are located at /tmp/ directory. You may redirect log output whereever you wanna, just use readme of glog (be careful. Official repo is 0.5.0 version, but we have 0.4.0 until https://github.com/conan-io/conan-center-index/issues/7772 is not fixed).

If you want to print all logs also into your terminal, than run app as:
    $ GLOG_logtostderr=1 ./surcha

*************************************