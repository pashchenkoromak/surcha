#!/usr/bin/bash

#this ensures newer versions of gcc will not be used with cmake
if command -v gcc-9 > /dev/null 2>&1; then
export CC=$(command -v gcc-9)
fi
if command -v g++-9 > /dev/null 2>&1; then
export CXX=$(command -v g++-9)
fi

rm -rf ./bin
mkdir -p bin && cd bin

conan install .. -pr=../conanprofile.txt

cmake ..
cmake --build . -- -j `nproc`
