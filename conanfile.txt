[requires]
nlohmann_json/3.9.1
gtest/1.11.0
glog/0.4.0
magic_enum/0.7.3

[generators]
cmake
