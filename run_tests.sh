#!/usr/bin/bash
mkdir -p bin && cd bin

conan install .. -pr=../conanprofile.txt

cmake ..
cmake --build . --target tests -- -j `nproc`



./bin/tests

lcov  --capture --directory . --output-file coverage.info --quiet
lcov --quiet -r coverage.info *.conan* */build/* */tests/* */c++/* -o coverageFiltered.info

gcovr -r ../ . --html --html-details -o report.html --exclude-directories='.*/tests/'
